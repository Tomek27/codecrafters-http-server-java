package controller;

import http.*;

public class UserAgentController {

  private UserAgentController() {
  }

  public static HttpResponse handleUserAgent(HttpRequest request) {
    String userAgent = request.getHeaders().getOrDefault("User-Agent", "");
    return HttpResponse.builder()
        .status(HttpStatus.OK)
        .contentType(ContentType.TEXT_PLAIN)
        .content(userAgent.getBytes(Http.CHARSET))
        .build();
  }
}
