package controller;

import http.*;

public class EchoController {

  private EchoController() {
  }

  public static HttpResponse handleEcho(HttpRequest request) {
    String variable = extractPathVariable(request.getPath());
    return HttpResponse.builder().status(HttpStatus.OK).contentType(ContentType.TEXT_PLAIN)
        .content(variable.getBytes(Http.CHARSET)).build();
  }

  private static String extractPathVariable(String path) {
    if (path != null) {
      return path.replaceFirst("/echo/", "");
    }
    return "";
  }
}
