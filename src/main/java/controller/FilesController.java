package controller;

import config.Config;
import http.*;
import log.Log;
import lombok.AllArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@AllArgsConstructor
public class FilesController {

  private static final Log log = Log.getInstance();

  private final Config config;

  private static String extractPathVariable(String path) {
    if (path != null) {
      return path.replaceFirst("/files/", "");
    }
    return "";
  }

  public HttpResponse handleRequest(HttpRequest request) throws IOException {
    return switch (request.getMethod()) {
      case "GET" -> getFiles(request);
      case "POST" -> postFiles(request);
      default -> HttpResponse.METHOD_NOT_ALLOWED;
    };
  }

  private HttpResponse getFiles(HttpRequest request) throws IOException {
    String fileName = extractPathVariable(request.getPath());
    log.info("Fetching file...: %s", fileName);
    try (FileInputStream inputStream = FileUtils.openInputStream(new File(config.getDirectory(), fileName))) {
      byte[] data = IOUtils.toByteArray(inputStream);
      return HttpResponse.builder().status(HttpStatus.OK).contentType(ContentType.APP_OCTET_STREAM)
          .content(data).build();
    } catch (FileNotFoundException ex) {
      log.warn(ex, "File not found: %s", fileName);
      return HttpResponse.NOT_FOUND;
    }
  }

  private HttpResponse postFiles(HttpRequest request) throws IOException {
    String fileName = extractPathVariable(request.getPath());
    log.info("Uploading file...: %s", fileName);
    try {
      var ouptputFile = new File(config.getDirectory(), fileName);
      FileUtils.write(ouptputFile, request.getBody(), Http.CHARSET);
      log.info("File uploaded: %s", fileName);
      return HttpResponse.CREATED;
    } catch (IllegalArgumentException ex) {
      log.error(ex, "File could not be written: %s", fileName);
      return HttpResponse.INTERNAL_SERVER_ERROR;
    }
  }
}
