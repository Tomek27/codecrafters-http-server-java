package config;

import log.Log;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import static config.Options.*;

@Getter
@ToString
public class Config {

  private static final Log log = Log.getInstance();

  private int port = 4221;
  private int threads = 10;
  private String directory = "";
  private int requestLength = 1024;

  private Config() {
  }

  public static Config parse(String[] args) {
    CommandLine commandLine = Options.parse(args);
    Config config = new Config();

    Integer port = parsedValue(commandLine, PORT, Integer.class);
    if (port != null) config.port = port;

    Integer threads = parsedValue(commandLine, THREADS, Integer.class);
    if (threads != null) config.threads = threads;

    String directory = parsedValue(commandLine, DIRECTORY, String.class);
    if (directory != null) config.directory = directory;

    Integer requestLength = parsedValue(commandLine, REQUEST_LENGTH, Integer.class);
    if (requestLength != null) config.requestLength = requestLength;

    return config;
  }

  private static <T> T parsedValue(CommandLine commandLine, Option option, Class<T> clazz) {
    if (commandLine.hasOption(option)) {
      try {
        Object value = commandLine.getParsedOptionValue(option);
        return clazz.cast(value);
      } catch (ParseException e) {
        log.error(e, "Parsing value of option: %s", option);
      }
    }
    return null;
  }
}
