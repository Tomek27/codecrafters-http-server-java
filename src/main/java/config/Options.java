package config;

import log.Log;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

public class Options extends org.apache.commons.cli.Options {

  public static final Option PORT = Option.builder("p")
      .required(false)
      .desc("Port to listen")
      .longOpt("port")
      .hasArg()
      .type(Integer.class)
      .build();

  public static final Option THREADS = Option.builder("t")
      .required(false)
      .desc("Level of parallelism for threading")
      .longOpt("threads")
      .hasArg()
      .type(Integer.class)
      .build();

  public static final Option DIRECTORY = Option.builder("d")
      .required(false)
      .desc("Root directory to fetch files")
      .longOpt("directory")
      .hasArg()
      .type(String.class)
      .build();

  public static final Option REQUEST_LENGTH = Option.builder("l")
      .required(false)
      .desc("Max request length (Content-Length) allowed")
      .longOpt("length")
      .hasArg()
      .type(Integer.class)
      .build();

  private static final Log log = Log.getInstance();

  private Options() {
    addOption(PORT);
    addOption(THREADS);
    addOption(DIRECTORY);
    addOption(REQUEST_LENGTH);
  }

  public static CommandLine parse(String[] args) {
    try {
      var parser = new DefaultParser();
      return parser.parse(new Options(), args);
    } catch (ParseException ex) {
      log.error(ex, "Command arguments");
      return new CommandLine.Builder().build();
    }
  }
}
