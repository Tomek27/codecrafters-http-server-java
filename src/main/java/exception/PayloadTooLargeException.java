package exception;

public class PayloadTooLargeException extends RuntimeException {

  public PayloadTooLargeException(int maxPayload, int requestedPayload) {
    super("Max payload of " + maxPayload + " exceeded. Requested payload was "
        + requestedPayload);
  }
}
