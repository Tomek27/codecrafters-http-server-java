package log;

public class Log {

  private static Log INSTANCE;

  private Log() {
  }

  public static Log getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new Log();
    }
    return INSTANCE;
  }

  public void info(String msg, Object... args) {
    log("[INFO]", null, msg, args);
  }

  public void warn(Throwable t, String msg, Object... args) {
    log("[WARN]", null, msg, args);
  }

  public void error(Throwable t, String msg, Object... args) {
    String stacktrace = t != null ? t.toString() : null;
    log("[ERROR]", stacktrace, msg, args);
  }

  public void error(String msg, Object... args) {
    error(null, msg, args);
  }

  protected void log(String level, String nextLine, String message, Object... args) {
    System.out.println(level + " " + String.format(message, args));
    if (nextLine != null) {
      System.out.println(nextLine);
    }
  }
}
