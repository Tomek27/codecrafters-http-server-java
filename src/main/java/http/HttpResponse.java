package http;

import lombok.*;
import org.apache.commons.lang3.ArrayUtils;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpResponse {

  public static final HttpResponse OK = fromStatus(HttpStatus.OK);
  public static final HttpResponse CREATED = fromStatus(HttpStatus.CREATED);
  public static final HttpResponse NOT_FOUND = fromStatus(HttpStatus.NOT_FOUND);
  public static final HttpResponse METHOD_NOT_ALLOWED = fromStatus(HttpStatus.METHOD_NOT_ALLOWED);
  public static final HttpResponse PAYLOAD_TOO_LARGE = fromStatus(HttpStatus.PAYLOAD_TOO_LARGE);
  public static final HttpResponse INTERNAL_SERVER_ERROR = fromStatus(HttpStatus.INTERNAL_SERVER_ERROR);

  private HttpStatus status;
  private ContentType contentType;
  @Setter(AccessLevel.NONE)
  private byte[] content;

  private static void appendRespondLine(StringBuilder s, HttpStatus status) {
    s.append(Http.PROTOCOL);
    s.append(' ');
    s.append(status);
    appendClRf(s);
  }

  private static void appendContentType(StringBuilder s, ContentType type) {
    if (type != null) {
      s.append("Content-Type: ");
      s.append(type);
      appendClRf(s);
    }
  }

  private static void appendContentLength(StringBuilder s, byte[] content) {
    s.append("Content-Length: ");
    if (content != null) {
      s.append(content.length);
    } else {
      s.append(0);
    }
    appendClRf(s);
  }

  private static void appendClRf(StringBuilder s) {
    s.append(Http.CLRF);
  }

  private static HttpResponse fromStatus(HttpStatus status) {
    return HttpResponse.builder().status(status).build();
  }

  public byte[] toByteData() {
    StringBuilder s = new StringBuilder();
    appendRespondLine(s, status);
    appendContentType(s, contentType);
    appendContentLength(s, content);
    appendClRf(s);
    byte[] responseHeaders = s.toString().getBytes(Http.CHARSET);
    byte[] contentData = content != null ? content : new byte[0];
    return ArrayUtils.addAll(responseHeaders, contentData);
  }

  @Override
  public String toString() {
    StringBuilder s = new StringBuilder();
    appendRespondLine(s, status);
    appendContentType(s, contentType);
    appendContentLength(s, content);
    return s.toString();
  }
}
