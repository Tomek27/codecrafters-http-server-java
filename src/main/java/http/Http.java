package http;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Http {

  public static final String PROTOCOL = "HTTP/1.1";

  public static final String CLRF = "\r\n";
  public static final Charset CHARSET = StandardCharsets.UTF_8;

  private Http() {
  }
}
