package http;

public enum ContentType {
  TEXT_PLAIN("text/plain"),
  APP_OCTET_STREAM("application/octet-stream");

  private final String name;

  ContentType(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }
}
