package http;

import exception.PayloadTooLargeException;
import log.Log;
import lombok.Builder;
import lombok.Data;
import util.StringUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class HttpRequest {
  private static final Log log = Log.getInstance();

  private static final String HEADER_KEY_VALUE_SEPARATOR = ":\\s*";

  private Map<String, String> headers;
  private String method;
  private String path;
  private String protocol;
  private String body;

  private int maxRequestLength;

  public HttpRequest(BufferedReader reader, int maxRequestLength) throws IOException {
    this.maxRequestLength = maxRequestLength;
    extractRequestLine(reader.readLine());
    extractHeaders(reader);
    extractBody(reader);
  }

  @Builder
  public HttpRequest(String method, String path, String protocol) {
    setMethod(method);
    setPath(path);
    setProtocol(protocol);
  }

  private void extractRequestLine(String requestLine) {
    if (requestLine != null) {
      String[] parts = requestLine.split("\\s");
      if (parts.length >= 2) {
        method = parts[0];
        path = parts[1];
        protocol = parts[2];
      } else {
        throw new IllegalArgumentException("Request line has no all 3 parts: method, path, protocol. Received: "
            + requestLine);
      }
    } else {
      throw new IllegalArgumentException("Request line was null");
    }
  }

  private void extractHeaders(BufferedReader reader) throws IOException {
    List<String> lines = new ArrayList<>();
    String line = reader.readLine();
    while (StringUtil.isNotBlank(line)) {
      lines.add(line);
      line = reader.readLine();
    }
    fillHeaders(lines);
  }

  private void extractBody(BufferedReader reader) throws IOException {
    int contentLength = getContentLength();
    if (contentLength > 0) {
      if (contentLength > maxRequestLength) {
        throw new PayloadTooLargeException(maxRequestLength, contentLength);
      }
      char[] reqBody = new char[contentLength];
      int totalRead = reader.read(reqBody, 0, contentLength);
      body = new String(reqBody);
      log.info("End extracting body with total read: %d", totalRead);
    }
  }

  private int getContentLength() {
    if (headers != null) {
      var value = headers.getOrDefault("Content-Length", "");
      try {
        if (StringUtil.isNotBlank(value)) {
          return Integer.parseInt(value);
        }
      } catch (NumberFormatException e) {
        log.error(e, "Content-Length wrong format: %s", value);
      }
    }
    return 0;
  }

  void fillHeaders(List<String> lines) {
    if (headers == null) {
      headers = new HashMap<>();
    }

    // Skipping request line
    for (int i = 1; i < lines.size(); ++i) {
      String line = lines.get(i);
      if (line != null && !line.trim().isBlank()) {
        String[] split = line.split(HEADER_KEY_VALUE_SEPARATOR, 2);
        if (split.length == 1) {
          headers.put(split[0], "");
        } else {
          headers.put(split[0], split[1]);
        }
      }
    }
  }
}
