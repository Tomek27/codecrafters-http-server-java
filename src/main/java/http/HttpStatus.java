package http;

public enum HttpStatus {
  OK(200),
  CREATED(201),
  NOT_FOUND(404),
  METHOD_NOT_ALLOWED(405),
  PAYLOAD_TOO_LARGE(413),
  INTERNAL_SERVER_ERROR(500);

  private final int code;

  HttpStatus(int code) {
    this.code = code;
  }

  @Override
  public String toString() {
    return code + " " + name().replaceAll("_", "");
  }
}
