package util;

import java.util.Objects;

public class StringUtil {

  private StringUtil() {
  }

  public static boolean isBlank(String s) {
    return Objects.toString(s, "").isBlank();
  }

  public static boolean isNotBlank(String s) {
    return !isBlank(s);
  }
}
