import config.Config;
import handler.RequestHandler;
import log.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

  private static final Log log = Log.getInstance();

  public static void main(String[] args) {
    Config config = Config.parse(args);
    log.info("Config parsed: %s", config);

    try (ExecutorService threadPool = Executors.newFixedThreadPool(config.getThreads())) {

      try (var serverSocket = new ServerSocket(config.getPort())) {
        serverSocket.setReuseAddress(true);

        while (true) {
          var clientSocket = serverSocket.accept();
          log.info("Connection accepted from: %s", clientSocket.getInetAddress());
          RequestHandler handler = new RequestHandler(config, clientSocket.getInputStream(),
              clientSocket.getOutputStream());
          threadPool.submit(handler);
        }
      } catch (IOException ex) {
        log.error(ex, "Connecting to server");
      }
    }
  }
}