package handler;

import config.Config;
import controller.EchoController;
import controller.FilesController;
import controller.UserAgentController;
import exception.PayloadTooLargeException;
import http.HttpRequest;
import http.HttpResponse;
import log.Log;
import lombok.AllArgsConstructor;

import java.io.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
public class RequestHandler implements Runnable {

  private static final Log log = Log.getInstance();

  private final UUID uuid = UUID.randomUUID();
  private final OffsetDateTime createdOn = OffsetDateTime.now();

  private final Config config;
  private final InputStream requestStream;
  private final OutputStream responseStream;

  @Override
  public void run() {
    log.info("Start request: %s", uuid);
    try {
      try {
        var request = handleRequest(requestStream);

        if ("/".equals(request.getPath())) {
          handleResponse(responseStream, HttpResponse.OK);
        } else if (request.getPath().startsWith("/echo/")) {
          handleResponse(responseStream, EchoController.handleEcho(request));
        } else if (request.getPath().startsWith("/user-agent")) {
          handleResponse(responseStream, UserAgentController.handleUserAgent(request));
        } else if (request.getPath().startsWith("/files/")) {
          FilesController filesController = new FilesController(config);
          HttpResponse response = filesController.handleRequest(request);
          handleResponse(responseStream, response);
        } else {
          handleResponse(responseStream, HttpResponse.NOT_FOUND);
        }
      } catch (PayloadTooLargeException ex) {
        log.warn(ex, "Requested payload for: %s", uuid);
        handleResponse(responseStream, HttpResponse.PAYLOAD_TOO_LARGE);
      }
    } catch (IOException ex) {
      log.error(ex, "Handling request %s", uuid);
    }
    log.info("End request: %s", uuid, createdOn);
  }

  private HttpRequest handleRequest(InputStream input) throws IOException {
    log.info("Handle request: %s", uuid);
    var reader = new BufferedReader(new InputStreamReader(input));
    return new HttpRequest(reader, config.getRequestLength());
  }

  private void handleResponse(OutputStream output, HttpResponse response) throws IOException {
    log.info("Sending respond: %s\n%s", uuid, response);
    output.write(response.toByteData());
    output.flush();
    output.close();
  }
}
