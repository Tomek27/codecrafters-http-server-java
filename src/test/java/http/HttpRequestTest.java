package http;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled
class HttpRequestTest {

  private static final String REQUEST_LINE = "GET / HTTP/1.1";

  private static HttpRequest createRequest() {
    return HttpRequest.builder()
        .method("GET")
        .path("/")
        .protocol(Http.PROTOCOL).build();
  }

  @Test
  @DisplayName("""
      GIVEN on header User-Agent: Firefox\s
      WHEN headers are filled\s
      THEN the mapping contains that one header
      """)
  void fillHeaders_1() {
    var req = createRequest();

    req.fillHeaders(List.of(REQUEST_LINE, "User-Agent: Firefox"));

    assertNotNull(req.getHeaders());
    assertEquals(1, req.getHeaders().size());
    assertEquals("Firefox", req.getHeaders().get("User-Agent"));
  }

  @Test
  @DisplayName("""
      GIVEN one blank User-Agent:\s
      WHEN headers are filled\s
      THEN the mapping contains that one header with empty string
      """)
  void fillHeaders_2() {
    var req = createRequest();

    req.fillHeaders(List.of(REQUEST_LINE, "User-Agent: "));

    assertNotNull(req.getHeaders());
    assertEquals(1, req.getHeaders().size());
    assertEquals("", req.getHeaders().get("User-Agent"));
  }

  @Test
  @DisplayName("""
      GIVEN one empty User-Agent:\s
      WHEN headers are filled\s
      THEN the mapping contains that one header with empty string
      """)
  void fillHeaders_3() {
    var req = createRequest();

    req.fillHeaders(List.of(REQUEST_LINE, "User-Agent:"));

    assertNotNull(req.getHeaders());
    assertEquals(1, req.getHeaders().size());
    assertEquals("", req.getHeaders().get("User-Agent"));
  }
}
