package controller;

import http.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled
class EchoControllerTest {

  private static HttpRequest createRequest(String path) {
    return HttpRequest.builder()
        .method("GET")
        .path(path)
        .build();
  }

  private static HttpResponse createResponse(String content) {
    return HttpResponse.builder()
        .status(HttpStatus.OK)
        .contentType(ContentType.TEXT_PLAIN)
        .content(content.getBytes(Http.CHARSET))
        .build();
  }

  @Test
  @DisplayName("""
      GIVEN path /echo/rofl\s
      WHEN its controller is called\s
      THEN response content is: 'rofl'
      """)
  void handleEcho_1() {
    var req = createRequest("/echo/rofl");

    var resp = EchoController.handleEcho(req);

    assertEquals(createResponse("rofl"), resp);
  }

  @Test
  @DisplayName("""
      GIVEN path /echo/more/separated/paths\s
      WHEN its controller is called\s
      THEN response content is: 'more/separated/paths'
      """)
  void handleEcho_2() {
    var req = createRequest("/echo/more/separated/paths");

    var resp = EchoController.handleEcho(req);

    assertEquals(createResponse("more/separated/paths"), resp);
  }

  @Test
  @DisplayName("""
      GIVEN path /echo/\s
      WHEN its controller is called\s
      THEN response content is: ''
      """)
  void handleEcho_3() {
    var req = createRequest("/echo/");

    var resp = EchoController.handleEcho(req);

    assertEquals(createResponse(""), resp);
  }

  @Test
  @DisplayName("""
      GIVEN path /echo/\s
      WHEN its controller is called\s
      THEN response content is: 'monkey/humpty'
      """)
  void handleEcho_4() {
    var req = createRequest("/echo/monkey/humpty");

    var resp = EchoController.handleEcho(req);

    assertEquals(createResponse("monkey/humpty"), resp);
  }
}
