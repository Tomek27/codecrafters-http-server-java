package config;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OptionsTest {

  @Test
  @DisplayName("""
      GIVEN --port 1337 argument\s
      WHEN parsed\s
      THEN the config.port has the parsed value
      """)
  void parse_1() {
    String[] args = {"--port", "1337"};

    var commandLine = Options.parse(args);

    assertNotNull(commandLine);
    assertTrue(commandLine.hasOption('p'));
    assertTrue(commandLine.hasOption("port"));
    assertEquals("1337", commandLine.getOptionValue("port"));
  }
}
